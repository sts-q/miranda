# Miranda

Miranda, the beautiful, the beautiful mother of this crazy Haskell.



# Links

[ Miranda mirror at github by ncihnegn ]( https://github.com/ncihnegn/miranda )

[ Miranda ]( https://www.cs.kent.ac.uk/people/staff/dat/miranda/ )
 -- A Non-strict Polymorphic Functional Language.
 -- Link not working any more?

[ David Turner ]( https://en.wikipedia.org/wiki/David_Turner_(computer_scientist) )
at wikipedia.

[ Comparision of functional programming languages. ]( https://wiki.haskell.org/Comparison_of_functional_programming_languages )

[ David Turner with Miranda Updates 2020. ]( https://www.youtube.com/watch?v=O_WqZAfIHRM )
-- David Turner at Lambda Days 2020 with a 5 minutes introduction 
and invitation for the now open sourced Miranda.

[ David Turner: Open Sourcing Miranda ]( https://www.youtube.com/watch?v=ZkLE0jrflwQ )

[ krc ](https://codeberg.org/martinwguy/krc )
-- KRC, a precursor to Miranda, is a simple lazy functional language designed and implemented by David Turner around 1979-81.


[ miranda.lua ]( https://codeberg.org/sts-q/vis-editor/src/branch/master/miranda.lua )
-- Miranda syntax highlighting for editor vis.



## Books

[ Chris Clack, Colin Myers, Ellen Poon: Programming with Miranda (chapter 1+2)]( http://www0.cs.ucl.ac.uk/teaching/3C11/book/Ch1-2.pdf ),
index page with links to [ all chapters ]( http://www0.cs.ucl.ac.uk/teaching/3C11/book/book.html ).

[ Ian Holyer: Functional programming with Miranda ](https://archive.org/details/functionalprogra0000holy/mode/2up )
-- University College London 1991, 228 pages.

[ Simon Thompson: Miranda -- The Craft of Functional Programming]( https://www.cs.kent.ac.uk/people/staff/sjt/Miranda_craft/ )

Ralf Hinze: Einführung in die funktionale Programmierung mit Miranda. B.G.Teubner Stuttgart 1992, 345 pages.
-- (books-on-demand)

# Contact

[ sts-q ](https://sts-q.codeberg.page/ )

This repository was created at 2022-08-01.

