|| ============================================================================
||
|| :file: test-silent.m
||
|| ============================================================================
|| dict ||
%include "lib.m"
%include "devel.m"

|| ============  usage:
||      concat testsText with toplevel output text
||
||      print testsText:
||              (layTests__)

testsText = [okMess ],               if no failed
          = [dotAs Error ++ line],   otherwise
            where
            line               = swoncat (foldl conc "" failed)  "############  Failed tests: "
            failed             = filter f tests
            f (name, t)        = t = False
            conc acc (name, t) = acc ++ name ++ "   "
            okMess = "test-silent: All tests passed. (" ++ shownum (#tests) ++ ")"

layTests__      = lay ["All tests passed."],           if testsText = []
                = lay (testsText ++ [(dotAs Text)]),   otherwise


|| ============================================================================
|| :chapter: TESTS
div_mod_ok a b  =  0 <=  a mod b  <  b,   if b > 0
                =  b <   a mod b  <= 0,   if b < 0

test_while__ = [
                  test_while_1__ = 20,
                  test_while_2__ = [21,7,8,9,10],
                  test_while_3__ = (21, [7,8,9,10]),
                  test_while_4__ = [101,102,103,104,105,6,7,8,9,10]
               ]

test_while_1__ =
                while p f 25
                where
                p n = n > 20
                f n = n - 1

test_while_2__ =
                while p f [1,2,3,4,5,6,7,8,9,10]
                where
                p (x:xs) = x <= 15
                f (x:y:xs) = (x + y : xs)

test_while_3__ =
                while p f (start, [1,2,3,4,5,6,7,8,9,10])
                where
                p (acc, x : xs) = x <= 6
                f (acc, x : xs) = (acc + x, xs)
                start = 0

test_while_4__ =
                head ++ tail
                where
                (head, tail) = while p f ([], [1,2,3,4,5,6,7,8,9,10])
                p (acc, x : xs) = x <= 5
                f (acc, x : xs) = (acc ++ [x + 100], xs)


tests = [

|| ----------------------------------------------------------------------------
|| :chapter: DEFINED INTERNALY
|| :section:    operators
        || ("---0",   [] -- [] = [] ),                          || type error: = binds stronger than --
        ("---1",   ([1,2,3,4,5,6,7,8,9,0] -- [2,4,6,8,0] -- [2,4]) = [1,2,3,4,5,7,9]),
        ("++-1",   (False = False:[]) = [True]),                || = binds stronger than :
        ("div-mod-1",   and [ a div b = entier (a/b)        | a,b <- [-15..15]; b ~= 0]   = True),
        ("div-mod-2",   and [ b * (a div b) + a mod b = a   | a,b <- [-15..15]; b ~= 0]   = True),
        ("div-mod-3",   and [ div_mod_ok a b                | a,b <- [-15..15]; b ~= 0]   = True),


|| :section:    math functions
        ("arctan",      True),
        ("cos",         True),
        ("exp",         True),
        ("log",         True),
        ("log10",       True),
        ("sin",         True),
        ("sqrt",        True),


|| :section:    functions
        ("code-1",      code 'a' = 97),
        ("decode-1",    decode 97 = 'a'),
        ("drop-1",      drop 3 [1,2,3,4,5,6] = [4,5,6]),
        ("entier-1",    entier 1.0 = 1),
        ("entier-1",    entier 3.5 = 3),
        ("entier-1",    entier (-3.5) = -4),
        || ("error",       error "mess" = 3),                   || program aborts
        || ("filemode-1",  filemode "lib.m" = "-rw-"),          || ok
        ("foldl-1",     foldl (+) 0 [1,2,3,4] = 10),
        ("foldl1-1",    foldl1 (+) [1,2,3,4]  = 10),
        ("foldr-1",     foldr  (-) 0 [1,2,3]  =  2),
        ("force-1",     force 1 = 1),
        ("getenv",      getenv "CC" = "gcc"),
        ("hugenum",     hugenum = hugenum),
        || ("integer-1",   integer 'a' ...),                       || type error: 
        ("integer-2",   integer 3 = True),
        ("integer-3",   integer pi = False),
        ("last-2",      last [1,2,3,4] = 4),
        ("merge",       merge [2,4,6,8] [1,3,5,7,8] = [1,2,3,4,5,6,7,8,8]),
        ("numval-1",    numval "   3.1415" = 3.1415),
        ("numval-2",    numval "  100123"  = 100123),
        ("numval-3",    numval "  1.00123E6" = 1001230),
        || ("read-1",      read ...),
        ("seq-1",       seq 1 2 = 2),
        ("shownum-1",   (shownum 123) = "123"),
        ("showfloat-1", (shownum 123.123) = "123.123"),
        ("showscaled-1",(showscaled  3 123.123) = "1.231e+02"),  
        ("system-1",    system "uname" = ("Linux\n","",0)),
        ("zip2",        zip2 [0..3] "type" = [(0,'t'), (1,'y'), (2,'p'), (3,'e')]),
        ("take-1",      take 3 [1,2,3,4,5,6] = [1,2,3]),
        ("tinynum",     tinynum = tinynum),


|| ----------------------------------------------------------------------------
|| :chapter:    DEFINED IN LIBS
|| :section:    Math
         ("fac-0", fac 0 = 1),
         ("fac-1", fac 3 = 6),
         ("fac-2", fac 5 = 120),
         ("fac-3", fac 7 = 5040),

         ("fib-12", fib 12 = 144),
         ("fib-30", fib 15 = 610),
         ("fib-30", fibs!42 = 267914296),
         ("fib-30", fibs!15 = fib 15),

         ("ack-3-3", ack 3 3 = 61),
  
         ("gcd-1", gcd 12  4 = 4),
         ("gcd-2", gcd 12 15 = 3),
         ("gcd-3", gcd 13 17 = 1),

         ("sgn-1", sgn   123  =   1),
         ("sgn-2", sgn     0 =    0),
         ("sgn-3", sgn (-123) = (-1)),

         ("lcm-1!", lcm 12 18 = 36),

         ("arith_mean-1", arith_mean [1,2,3,4,5] = 3),

         ("geom_mean-1", entier (geom_mean [200,200,300,400,300]) = 270),

|| ----------------------------------------------------------------------------
|| :section:    Combinators
        ("compose_23-1", (compose22 (+)(+))         1 2 3               = 6),
        ("compose_23-2", (compose22 (++)(++))       "abc" "def" "ghi"   = "abcdefghi"),
        ("compose_21_1", (compose21 (+)(+100))      10 1                = 111),
        ("compose_21_1", (compose21 (++)(++".." ))  "abc" "def"         = "abc..def"),
        ("dip-1",  (dip (-)(+)) 1 2 3                                   = -4),
        ("dip-2",  (dip (converse (:)) (++)) ["abc"] "123" "456"        = ["123456", "abc"]),

        ("while 1",     while (>20) (subtract 1) 25     = 20),
        ("while test_while__",  and test_while__ = True),


|| ----------------------------------------------------------------------------
|| :section:    Lists

        ("swoncat-1",   swoncat [1,2,3] [4,5,6]  = [4,5,6,  1,2,3]),
        ("appendToLast-0", (appendToLast "..>" []) = ["..>"]),
        ("appendToLast-1", (appendToLast "..>" ["abc", "def", "ghi"]) = ["abc", "def", "ghi..>"]),
        ("appendToLast-2", (appendToLast [11,22,33]  [[1,2,3]]) = [[1,2,3,11,22,33]]),

        ("inconcat",   (inconcat [22,33] [[1,2,3], [4,5,6]]) = [1,2,3, 22,33 ,4,5,6]),

        ("hasSubseq-1", (hasSubseq "otto" "ottomax")    = True),
        ("hasSubseq-2", (hasSubseq "ottm" "ottomax")    = False),
        ("hasSubseq-2", (hasSubseq "ottm" "ottomax")    = False),
        ("hasSubseq-3", (hasSubseq "otto" "abcdottomax") = True),
        ("hasSubseq-4", (hasSubseq "otto" "abcdefotto") = True),
        ("hasSubseq-5", (hasSubseq "otto" "otto")       = True),
        ("hasSubseq-6", (hasSubseq "Otto" "otto")       = False),
        ("hasSubseq-7", (hasSubseq "otto" "tto")        = False),

        ("findSubseq-1",(findSubseq "otto" "")          = (False, "")),
        ("findSubseq-2",(findSubseq "otto" "otto")      = (True,  "otto")),
        ("findSubseq-3",(findSubseq "otto" "myottomax") = (True,  "ottomax")),
        ("findSubseq-4",(findSubseq "otto" "myottmax")  = (False, "")),
        ("findSubseq-5",(findSubseq "otto" "ottomax")   = (True,  "ottomax")),
        ("findSubseq-6",(findSubseq "otto" "myotto")    = (True,  "otto")),

        ("rive-1",      rive  0 [] = ([],[])),
        ("rive-2",      rive 10 [] = ([],[])),
        ("rive-3",      rive  0 [1,2,3,4] = ([],[1,2,3,4])),
        ("rive-4",      rive  4 [1,2,3,4] = ([1,2,3,4],[])),
        ("rive-5",      rive  1 [1,2,3,4] = ([1],[2,3,4])),

        ("rolldown-1",  times 3 rolldown [1,2,3,4,5,6,7] = [4,5,6,7,1,2,3]),
        ("rollup-2",    times 3 rollup   [1,2,3,4,5,6,7] = [5,6,7,1,2,3,4]),

        ("nub-1",       nub [1,2,3,4]                 =  [1,2,3,4]),
        ("nub-2",       nub [1,2,3,9,4,4,3,2,9,1,9]   =  [1,2,3,9,4]),

        ("assoc-1",     assoc 1 [(1,11),(2,22),(3,33)]  = Some (1,11)),
        ("assoc-2",     assoc 2 [(1,11),(2,22),(3,33)]  = Some (2,22)),
        ("assoc-3",     assoc 4 [(1,11),(2,22),(3,33)]  = None),
        
        ("assocwith-1", assocwith ((=1).fst)   [(1,11),(2,22),(3,33)]     = Some (1,11)),
        ("assocwith-2", assocwith ((=33).snd)  [(1,11),(2,22),(3,33)]     = Some (3,33)),
        ("assocwith-3", assocwith ((=)[1,2,3]) [[1,2],[1,2,3],[1,2,3,4]]  = Some [1,2,3]),

        ("dict_get-1",  dict_get [(1,11),(2,22),(3,33)] 1   = Some 11),
        ("dict_get-3",  dict_get [(1,11),(2,22),(3,33)] 3   = Some 33),
        ("dict_get-4",  dict_get [(1,11),(2,22),(3,33)] 4   = None),
        
        ("dict_keys",   dict_keys   [(1,11),(2,22),(3,33)]    = [1,2,3]),
        ("dict_values", dict_values [(1,11),(2,22),(3,33)]    = [11,22,33]),

        ("dict_getkey 1", dict_getKey [(1,11),(2,22),(3,33)] 3     = None),
        ("dict_getkey 2", dict_getKey [(1,11),(2,22),(3,33)] 33    = Some 3),

        ("uniq-1",      uniq [1,2,2,2,4,1,1,5,7,8,2,2]  = [1,2,4,1,5,7,8,2]),



|| ============================================================================
        ("ok",          True)   

        ]   || tests

|| ============================================================================
|| (c) sts-q 2022-Aug

