||_____________________________________________________________________________
||
|| :file: caldate.m
||
||_____________________________________________________________________________
mCaldate = "caldate"

%include "lib.m"

|| %export

|| ============  usage:

||_____________________________________________________________________________
|| :section:    types
day     == num
month   == num
year    == num
daynum  == num                                  || number of day, counting starts at 1904-1-1
caldate == (year,month,day)                     || calender date


||_____________________________________________________________________________
firstValidYear  =  1904
lastValidYear   =  2090
firstValidDayNum =    0
lastValidDayNum = 68300
monthLengths    = [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]   || indexing Jan with 1

isValidDayNum dn = True,    if firstValidDayNum <= dn <= lastValidDayNum
                 = False,   otherwise

isValidCalDate (year,month,day) =
                (firstValidYear <= year <= lastValidYear)
                & (1 <= month <= 12)
                & (1 <= day <= monthLength year month)

caldateval :: [char] -> [char] -> [char] -> (year,month,day)
caldateval year month day =
                caldate
                where
                y = numval year
                m = numval month
                d = numval day
                caldate = (y,m,d),     if isValidCalDate (y,m,d)
                caldate = error mess,  otherwise
                          where
                          mess = "dateVal: not valid:  " ++ show (y,m,d)

           
||_____________________________________________________________________________
|| :section:    interface

isLeapYear :: year -> bool
isLeapYear year = True,    if year mod 400 = 0
                = False,   if year mod 100 = 0
                = True,    if year mod   4 = 0
                = False,   otherwise

monthLength :: year -> month -> num
monthLength year month
                = 29,                  if isLeapYear year & month = 2
                = monthLengths!month,  otherwise

readISODate :: string -> caldate
readISODate str = caldateval  (take 4 (drop 0 str))  (take 2 (drop 5 str))  (take 2(drop 8 str))

readDeDate :: string -> caldate
readDeDate  str = caldateval  (take 4 (drop 6 str))  (take 2 (drop 3 str))  (take 2(drop 0 str))

showISODate :: caldate -> string
showISODate (year,month,day) = shownum year ++ "-" ++ shownum00 month ++ "-" ++ shownum00 day

asDayNum :: caldate -> daynum
asDayNum cd   = error mess, if ~(isValidCalDate cd)
                where mess = "asDayNum: invalid calender date: " ++ show cd
asDayNum (year,month,day) =
                ydays + mdays + days - 1
                where
                ydays = 365 * (year - firstValidYear)  + leapYears
                mdays = sum (take (month - 1) mlens)
                days  = day
                leapYears = (year - 1 - firstValidYear) div 4
                mlens     = map (monthLength year) [1..month]

asCalDate :: daynum -> caldate
asCalDate dn =  error mess, if ~(isValidDayNum dn)
                where mess = "asCalDate: daynum not valid: " ++ shownum dn
asCalDate dn =  (y,m,d)
                where
                y = (dn + 1) * 100 div 36525 + firstValidYear
                r = dn - asDayNum(y,1,1) + 1
                mlens = map (monthLength y) [0..12]                     || subscribe Jan with 1, not with 0
                (m,d) = collect 1 r
                collect m r
                        = (m,r),                         if r <= mlens!m
                        = collect (m+1) (r - mlens!m),   otherwise

dateDiff :: caldate -> caldate -> num
dateDiff a b = asDayNum a  -  asDayNum b

dateAddDays :: caldate -> num -> caldate
dateAddDays cd n = asCalDate (asDayNum cd + n)

mkCalMonths :: (year,month) -> (year,month) -> day -> [caldate]
mkCalMonths a b day =
                error mess, if day > 28
                where mess = "mkCalMonths: day > 28:   " ++ shownum day
mkCalMonths (y1,m1) (y2,m2) day =
                filter valid many
                where
                many     =  [(y,m,day) | y <- [y1..y2]; m <- [1..12]]
                valid d  =  (y1,m1,day) <= d <= (y2,m2,day)


||_____________________________________________________________________________
|| :file: (c) sts-q 2022-Aug

