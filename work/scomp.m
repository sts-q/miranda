#! /home/heiko/bin/mira -exec
||_____________________________________________________________________________
|| :file: scomp.m                          A second shoot for a susel compiler.
|| susel:   SUper Simple Expression Language
|| here
|| susel/t: Totally SUperflous Expression Language
%include "lib"


||_____________________________________________________________________________
|| :chapter: TYPES
operator ::=                                                                    || #DOC operator: binary infix
                Add |Sub |Mul |Div |Mod |Fdiv |Pow                              || #DOC operator
                |Shl |Asr |Bitand |Bitor |Bitxor |Bitnot |Bitinv                || #DOC operator
                |And |Or |Xor                                                   || #DOC operator
                |Gt |GEql |Eql |NEql |Lt |LEql                                  || #DOC operator
                |NotFound                                                       || #DOC operator

builtin ::=     Sqr |Sqrt |Max |Min |IsEven |IsOdd |Not                         || #DOC builtin functions


token ::=                                                                       || #DOC types
                Char char                                                       || #DOC types
                | Number num                                                    || #DOC types
                | Bool bool                                                     || #DOC types
                | String string                                                 || #DOC types
                | Word string                                                   || #DOC types
                | Operator operator                                             || #DOC types
                | Builtin builtin                                               || #DOC types
                | Openparen | Closeparen                                        || #DOC types
                | Kwd_If |Kwd_Then |Kwd_Else |Kwd_For |Kwd_While                || #DOC types
                | Void                                                          || #DOC types
                | Seq [token]                                                   || #DOC types
                | Func [name] [token]                                           || #DOC types
                | NoIdea                                                        || #DOC types


tokens == [token]                                                               || #DOC types


binding     == (name,token)                                                     || #DOC types
box         == [binding]                                                        || #DOC types
environment == [box]                                                            || #DOC types


||_____________________________________________________________________________
|| :chapter: SHOW
showOperator :: operator -> string
showOperator op =
                s op where
                s Add   = "+"
                s Sub   = "-"
                s Mul   = "*"
                s Div   = "div"
                s Mod   = "mod"
                s Fdiv  = "/"
                s Pow   = "^"
                s Shl   = "<<"
                s Asr   = ">>"
                s Bitand = "/\\"
                s Bitor  = "\\/"
                s Bitxor = "><"
                s Bitinv = "inv"
                s And   = "and"
                s Or    = "or"
                s Xor   = "xor"
                s Gt    = ">"
                s GEql  = ">="
                s Eql   = "="
                s NEql  = "#"
                s Lt    = "<"
                s LEql  = "<="


showBuiltin :: builtin -> string
showBuiltin Sqr         = "sqr"
showBuiltin Sqrt        = "sqrt"
showBuiltin Max         = "max"
showBuiltin Min         = "min"
showBuiltin IsEven      = "even?"
showBuiltin IsOdd       = "odd?"
showBuiltin Not         = "not"
showBuiltin fun         = show fun


showToken :: token -> string
showToken t =
                        do t ++ " "
                        where
                        do (Number n)     = shownum n ++" "
                        do (Word w)       = w
                        do (Bool True)    = "true"
                        do (Bool False)   = "false"
                        do Openparen      = "("
                        do Closeparen     = ")"
                        do Kwd_If         = "IF"
                        do Kwd_Then       = "THEN"
                        do Kwd_Else       = "ELSE"
                        do Kwd_For        = "FOR"
                        do (Operator Add) = "+"
                        do (Operator Sub) = "-"
                        do (Func params def) = "  fn ("++ unwords params ++") "++ show def ++"  "
                        do t = "<" ++ show t ++ ">"



showBinding :: binding -> string
showBinding (name,value) =
                rjustify 25 name ++" : "++ fmt value where
                fmt (Number n) = rjustify 8 (shownum n)
                fmt (Bool True)  = "T"
                fmt (Bool False) = "FLS"
                fmt = showToken

showBox :: box -> text
showBox  = map showBinding

showEnv :: environment -> text
showEnv [] = []
showEnv    = (concat . (map (("---":). showBox)))

printEnv = lay . showEnv


||_____________________________________________________________________________
|| :chapter: LIB
spc :: string
spc = " "

char_isLetter           = char_isletter                                         || #DOC lib
char_isDigit            = char_isdigit                                          || #DOC lib
char_isWordStart        = char_isLetter                                         || #DOC lib
char_isWordTail c   = char_isLetter c \/ char_isDigit c \/ char_isWordExtra c   || #DOC lib
char_isWordExtra        = member "[]_/?"
char_isOperator         = member "+-*/\\<>=#~&|~"                               || #DOC lib

isOperator :: token -> bool                                                     || #DOC lib
isOperator (Operator o) = True
isOperator any = False

comesOperator :: tokens -> bool                                                 || #DOC lib
comesOperator ((Operator op) : ts) = True
comesOperator ts = False

isUnary  = member [Sqr, Sqrt, IsEven, IsOdd, Not]
isBinary = member [Max, Min]

isInt (Number x) = True                                                         || #DOC lib
isInt y = False

isBool (Bool x) = True                                                          || #DOC lib
isBool y = False



||_____________________________________________________________________________
|| :chapter: SOURCE
dirpath_susel = dirpath_home ++ "/q/oberon-07/susel/"

source :: string                                                                || #DOC source
source = read (dirpath_susel ++ "second.susel")


||_____________________________________________________________________________
|| :chapter: ENVIRONMENT
envir :: environment                                                            || #DOC envir
envir =
        [[
           ("otto", Word "Meier")
         ],[
           ("alen", Number 256),
           ("blen", Number 16)
        ]]

getValue :: environment -> name -> token
getValue (((key,value) : vs) : bs ) word = value, if key = word
getValue (( kvp        : vs) : bs ) word = getValue (vs : bs) word
getValue ([] :            (b : bs)) word = getValue (b : bs) word
getValue ([] : []) name = error ("getValue: name not found: " ++ name)
getValue []        name = error ("getValue: environment is empty")

setValue :: environment -> name -> token -> environment
setValue env name value =
                nenv where
                nenv = doBs env
                doBs (b : bs) = ((name,value) : (removeWith p b)) : bs, if member (map fst b) name
                doBs (b : bs) = b : doBs bs
                p (n,v) = name = n

isDefined:: environment -> name -> bool                                         || #DOC envir
isDefined env w = member (map fst (concat env)) w

define :: environment -> name -> token -> environment                           || #DOC envir
define env name value =
                nenv where
                nenv = consToFirst (name,value) env, if ~isDefined env name
                     = error ("define: already defined: "++ name), otherwise

let :: environment -> name -> token -> environment                              || #DOC envir
let env name value =
                nenv where
                nenv = setValue env name value, if isDefined env name
                     = error ("let: no such name defined: "++ name), otherwise

lookup :: environment -> name -> token                                          || #DOC envir
lookup = getValue


||_____________________________________________________________________________
|| :chapter: SCAN FUNS
skipLineComment []              = []
skipLineComment ('\n' : cs)     = cs
skipLineComment (c : cs)        = skipLineComment cs

readNumber res (c:cs) = readNumber (10 * res + numval [c]) cs,   if char_isDigit c
readNumber res cs     = (Number res, cs)

readWord w (c:cs)     = readWord (w ++ [c]) cs,   if char_isWordTail c
readWord w cs         = (Word w, cs)

orKwd (Word "if")       = Kwd_If
orKwd (Word "then")     = Kwd_Then
orKwd (Word "else")     = Kwd_Else
orKwd (Word "for")      = Kwd_For
orKwd (Word "while")    = Kwd_While
orKwd (Word "true")     = Bool True
orKwd (Word "false")    = Bool False

|| orKwd (Word "and")      = Operator And
orKwd (Word "or")       = Operator Or
orKwd (Word "xor")      = Operator Xor

orKwd (Word "div")      = Operator Div
orKwd (Word "mod")      = Operator Mod

orKwd (Word "sqr")      = Builtin Sqr
orKwd (Word "sqrt")     = Builtin Sqrt
orKwd (Word "max")      = Builtin Max
orKwd (Word "min")      = Builtin Min
orKwd (Word "even?")    = Builtin IsEven
orKwd (Word "odd?")     = Builtin IsOdd
orKwd (Word "not")      = Builtin Not
orKwd w                 = w

readOperator (c1:c2 : cs) =
                        (op c1 c2, cs),   if char_isOperator c2
                        where
                        op '*' '*' = Pow
                        op '>' '=' = GEql
                        op '<' '=' = LEql
                        op '<' '<' = Shl
                        op '>' '>' = Asr
                        op '\\' '/' = Bitor
                        op '/' '\\' = Bitand
                        op '>' '<'  = Bitxor
||                         op '-' ' '  = Sub
                        op c1 c2   = NotFound

readOperator (c : cs) =
                        (op c, cs)
                        where
                        op '+' = Add
                        op '-' = Sub
                        op '*' = Mul
                        op '/' = Fdiv
                        op '>' = Gt
                        op '<' = Lt
                        op '=' = Eql
                        op '#' = NEql
                        op '&' = And
                        op c   = NotFound

readString s ('"' :cs) = (s,cs)
readString s (c : cs)  = readString (s ++ [c]) cs

||_____________________________________________________________________________
|| :chapter: SCAN NEXT
tokenList :: string -> tokens                                                   || #DOC scan
tokenList []            = []
tokenList (' '  : cs)   = tokenList cs                                          || skip spaces
tokenList ('\n' : cs)   = tokenList cs                                          || skip newline
tokenList ('/':'/':cs)  = tokenList (skipLineComment cs)                        || skip line comment
tokenList ('(' : cs)    = Openparen  : tokenList cs                             || (
tokenList (')' : cs)    = Closeparen : tokenList cs                             || )

tokenList (c : cs)      = aNumber : tokenList rest,     if char_isDigit c       || read number
                          where (aNumber, rest) = readNumber 0 (c:cs)

tokenList (c : cs)      = orKwd aWord : tokenList rest, if char_isWordStart c   || read word or keyword
                          where (aWord, rest) = readWord "" (c:cs)

tokenList (c : cs)      = Operator aOp : tokenList rest,if char_isOperator c    || read operator
                          where (aOp, rest) = readOperator (c:cs)

tokenList ('"' : cs)    = String aString : tokenList rest                       || read string
                          where (aString, rest) = readString "" cs

tokenList ('\'':c:'\'':cs) = Char c : tokenList cs                              || read char

tokenList (c:cs)        = error (mess ++"<"++ [c] ++"> "++ take 12 cs)
                          where mess = "tokenList: unkown character: "


||_____________________________________________________________________________
|| :chapter: OPERATE
binop:: operator -> token -> token -> token
binop Add (Number n) (Number m) = Number (n + m)
binop Sub (Number n) (Number m) = Number (n - m)
binop Mul (Number n) (Number m) = Number (n * m)
binop Div (Number n) (Number m) = Number (n div m)
binop Mod (Number n) (Number m) = Number (n mod m)
binop Fdiv(Number n) (Number m) = Number (n / m)
binop Pow (Number n) (Number m) = Number (n ^ m)

binop Gt   (Number n) (Number m) = Bool (n >  m)
binop GEql (Number n) (Number m) = Bool (n >= m)
binop Eql  (Number n) (Number m) = Bool (n =  m)
binop NEql (Number n) (Number m) = Bool (n ~= m)
binop Lt   (Number n) (Number m) = Bool (n < m)
binop LEql (Number n) (Number m) = Bool (n <= m)

binop Eql  (Bool n) (Bool m) = Bool (n = m)
binop NEql (Bool n) (Bool m) = Bool (n ~= m)
binop And  (Bool n) (Bool m) = Bool (n & m)
binop Or   (Bool n) (Bool m) = Bool (n \/ m)
binop Xor  (Bool n) (Bool m) = Bool ((n \/ m) & ~(n & m))

binop fun a b = error ("binop not defined: "++ show fun ++" "++ showToken a ++ showToken b)


appUnary :: builtin -> token -> token
appUnary Sqr    (Number n) = Number (sqr n)
appUnary Sqrt   (Number n) = Number (sqrt n)
appUnary IsEven (Number n) = Bool (iseven n)
appUnary IsOdd  (Number n) = Bool (isodd n)
appUnary Not    (Number n) = Number (-n-1)
appUnary Not    (Bool b)   = Bool (~b)
appUnary fun val = error ("appUnary: op not defined: "++ show fun ++" "++ show val)

appBinary Max (Number n) (Number m) = Number (max [n,m])
appBinary Min (Number n) (Number m) = Number (min [n,m])
appBinary fun a b = error ("appBinary: op not defined: "++ show fun ++" "++ show a ++" "++ show b)


||_____________________________________________________________________________
|| :chapter: PARSE
parseParameter :: tokens -> environment -> ([string],tokens)                    || #DOC parse
parseParameter (Openparen : ts) env =
                (pnames, nts) where
                (pnames, nts) = do [] ts
                do ws (Closeparen : ts) = (ws, ts)
                do ws (Word w     : ts) = do (ws ++ [w]) ts
                do ws (t : ts) = error ("parseParameter: word expected: "++ show t)
                do ws [] = error ("parseParameter: word expected but not more tokens found.")
parseParameter ts env = error ("parseParameter: open paren expected")

parseDefinition :: tokens -> environment -> (tokens,tokens)                     || #DOC parse
parseDefinition (Openparen : ts) env =
                (def, nts) where
                (def, nts) = parse [] [] ts
                parse  stk        l (Openparen  : ts) = parse (Seq l:stk) [] ts
                parse (Seq x:stk) l (Closeparen : ts) = parse   stk   (Seq(reverse l):x)   ts
                parse  []         l (Closeparen : ts) = (reverse l, ts)
                parse stk l (t:ts) = parse stk (t:l) ts
parseDefinition (t:ts) env = error ("parseDefinition: open paren expected")
parseDefinition [] env = error ("parseDefinition: no more tokens to be found")


||_____________________________________________________________________________
|| :chapter: EVALUATE
factor :: tokens -> environment -> (token, tokens, environment)                 || #DOC eval

factor (Number n : ts) env = (Number n, ts, env)
factor (Bool b   : ts) env = (Bool b,   ts, env)

factor (Builtin sym : ts) env =
                (appUnary sym r, rest, nenv), if isUnary sym where
                (r, rest, nenv) = factor ts env

factor (Builtin sym : ts) env =
                (appBinary sym a b, rest2, nnenv), if isBinary sym where
                (a,rest1,nenv)  = factor ts env
                (b,rest2,nnenv) = factor rest1 nenv

factor (Kwd_If : ts) env =
                (r, nts, nenv) where
                (r, nts, nenv)                  = (last rs, rest3, env4)
                (testresult, rest1, env1)       = factor ts env
                (iftrue,     rest2)             = parseDefinition rest1 env1
                (iffalse,    rest3)             = parseDefinition rest2 env1
                (rs, [], env4)                  = eval iftrue  env1, if testresult = (Bool True)
                                                = eval iffalse env1, otherwise


factor (Word "Int" : Word w : ts) env =
                (r, nts, nenv) where
                (r, nts, env0) = factor ts env
                nenv = define env0 w r, if isInt r
                     = error ("factor/Int: type check failed: Int "++ w ++" |:= "++ show r), otherwise

factor (Word "Bool" : Word w : ts) env =
                (r, nts, nenv) where
                (r, nts, env0) = factor ts env
                nenv = define env w r, if isBool r
                     = error ("factor/Bool: type check failed: Bool "++ w ++" |:= "++ show r), otherwise

factor (Word "letint" : Word w : ts) env =
                (r, nts, nenv) where
                (r, nts, env0) = factor ts env
                nenv = let env0 w r, if isInt r
                     = error ("factor/letint: types don't match or Roadwork for [bool,...]"), otherwise

factor (Word "letbool" : Word w : ts) env =
                (r, nts, nenv) where
                (r, nts, env0) = factor ts env
                nenv = let env0 w r, if isBool r
                     = error ("factor/letbool: types don't match or Roadwork for [bool,...]"), otherwise

factor (Word "add" : Word w : ts) env =
                (r, nts, nenv) where
                (r, nts, env0) = factor ts env
                nenv = let env w (add (lookup env w) r)
                add (Number v) (Number r) = Number (v+r)
                add a b = error ("types don't match: add "++ w ++ " ("++ show r ++")")

factor (Word "inc" : Word w : ts) env =
                (r, ts, nenv) where
                r    = inc (lookup env w)
                nenv = let env w r
                inc (Number v) = Number (v+1)
                inc a = error ("types don't match: inc "++ w)

factor (Word "define" : Word w : ts) env =
                (r, nts, nenv) where
                (r, nts, nenv) = (func, rest2, define env w func)
                (params, rest1) = parseParameter  ts    env
                (def,    rest2) = parseDefinition rest1 env
                func = Func params def

factor (Word w : ts) env =
                (last r, ts, nenv) where
                (r,  nts, nenv) = (r1, ts, tl env1)         || keep going with ts
                (r1, ts1, env1) = g v env
                g (Func name seq) env = eval seq ([]:env)   || eval func
                g v               env = ([v],[],[]:env)     || cons anything else
                v = lookup env w, if isDefined env w
                v = error ("not defined: "++w), otherwise

factor (Openparen : ts) env =
                (r, nts, tl nenv) where
                (rs, rest, nenv) = eval ts ([] : env)
                (r, nts) = g (rs, rest)
                g (r:rs, Closeparen:ts) = (last (r:rs), ts)
                g (r:rs, ts) = error ("factor/openparen/g: closed paren expected")
                g ([],   ts) = error ("factor/[]/g: some result expected")

factor (t : ts) env = error ("factor: not found: "++ show t)
factor []       env = error ("factor: list of tokens is empty")


expr :: tokens -> environment -> (token, tokens, environment)                   || #DOC eval
expr ts env =   (z, nts,   nenv) where
                (x, rest1, env1) = factor ts env
                (z, nts,   nenv) = while p f (x, rest1, env1)
                p (x,ts,e) = some ts & isOperator (hd ts)
                f (x, Operator opSym : ts, e) =
                        (binop opSym x y, rest, ne) where
                        (y, rest, ne) = factor ts e
                f (x,ts,e) = error ("expr/f: (op : y : ts) expected.")


eval :: tokens -> environment -> (tokens, tokens, environment)                  || #DOC eval
eval ts env =   (results, rest, nenv) where
                (results, rest, nenv) = while p f ([], ts, env)
                p (rs,ts,e) = some ts & ~((hd ts) = Closeparen)
                f (rs,ts,e) =
                        (rs ++ [r], rest, ne) where
                        (       r,  rest, ne) = expr ts e


||_____________________________________________________________________________
|| :chapter: MAIN
theTokens :: tokens
theTokens = tokenList source                                                    || #DOC main
(results,rest, env1) = eval theTokens envir                                     || #DOC main

main :: [sys_message]                                                           || #DOC main
main =          map Stdout [
                     chapter "this is scomp",
||                      source,
||                      section "environment envir",    lay (showEnv envir),
||                      section "tokens",               show theTokens,
                     section "result",               lay (map showToken results),
                     section "environment env1",     lay (showEnv env1),
                     chapter "done", dotAs Std
                  ]

||_____________________________________________________________________________
|| :file: END (c) sts-q 2024-Jul,Aug
