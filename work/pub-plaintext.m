||_____________________________________________________________________________
||
|| :file: pub-plaintext.m
||
||__________________________  ___________________________________________________
%include "lib.m"
%include "local.m"
%include "xmlgen.m"
%include "pub.m"

%export pub_plaintext  pub_plaintext__


|| ============  usage:
||      (pub_plaintext "mytextfile")
||              read mytextfile
||              and write it to outfile
||
||      (pub_plaintext__)
||              read miranda.md and write to outfile
||
||_____________________________________________________________________________
outfile = dirpath_pub ++ "plaintext.html"


pub_plaintext :: filename -> [sys_message]      || write text file to pub/plaintext.html
pub_plaintext filename =
                docmess ++ usermess
                where
                docmess  = map  (Tofile outfile)  (doctypeXHtml : (xml_showTag html))
                usermess = [Stdout ("=======  pub plain text to file:" 
                                        ++ nl ++ "     " ++ filename 
                                        ++ nl ++ " --> " ++ outfile
                                        ++ nl)]
                html     =  (xhtml [
                              (xhead [
                                  (xmeta [("charset",   "utf-8")]),
                                  (xmeta [("generator", "Miranda-2.066")]),
                                  (xmeta [("at", today_now)]),
                                  (xtitle (outfile))
                              ]),
                              (xbody [
                                  (xinline_plaintext filename)
                                  ]) ])


pub_plaintext__ = pub_plaintext "../miranda.md"


||_____________________________________________________________________________
|| :file: (c) sts-q 2022-Aug

