||_____________________________________________________________________________
||
|| :file: pub.m
||
||_____________________________________________________________________________
%include "lib.m"
%include "xmlgen.m"

|| ============  usage:
||      pub a xml doc to file:
||              (pubDoc filename xml_doc)
||
||      print a xml doc to screen:
||              (printDoc xml_doc)
   
||_____________________________________________________________________________
doctypeHtml           = "<!DOCTYPE html>"

doctypeXHtml          = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\"   \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">"

printDoc doc          = lay (doctypeXHtml : (xml_showTag doc))

pubDoc filename doc   =
                docmess ++ usermess
                where
                docmess  = map (Tofile filename)   (doctypeXHtml : (xml_showTag doc))
                usermess = [Stdout ("=======  pub to file:" ++ nl ++ filename ++ nl)]


xhtml  ts          = Xtag "html"        []                      ts
xhead  ts          = Xtag "head"        []                      ts
xbody  ts          = Xtag "body"        []                      ts
xtitle name        = Xtag "title"       []                      [Xstr name]
xmeta  atts        = Xtag "meta"        atts                    []
xh1    line        = Xtag "h1"          []                      [Xstr line]
xh2    line        = Xtag "h2"          []                      [Xstr line]
xh3    line        = Xtag "h3"          []                      [Xstr line]
xp     ts          = Xtag "p"           []                      ts
xdiv   name ts     = Xtag "div"         [("id",name)]           ts
xa     dest name   = Xtag "a"           [("href",dest)]         [Xstr name]
xhr                = Xtag "hr"          []                      []
xbr                = Xtag "br"          []                      []
xpre   text        = Xtag "pre"         []                      (map (Xstr.(++nl)) text)
xdl    ts          = Xtag "dl"          []                      ts
xdt    ts          = Xtag "dl"          []                      ts
xdd    ts          = Xtag "dd"          []                      ts

xtext  text        = Xtag "p"           []                      (map (Xstr.(++nl)) text)

xmark_chapter name = Xmark_chapter name
xmark_section name = Xmark_section name
xmark_sep          = Xmark_separator


xinline_styleSheet file   = Xtag "style" [] [Xstr nl, Xstr (read file)]

xinline_plaintext filename = Xtag "pre" [] (map  (Xstr.(++nl))  ("":((lines.read) filename)))


||_____________________________________________________________________________
|| :file: (c) sts-q 2022-Aug

