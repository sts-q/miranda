||_____________________________________________________________________________
|| :file: devel.m
||
|| evals ||
||_____________________________________________________________________________
%include "lib.m"
%include "items.m"
%include "caldate.m"


mDevel = "devel"
mDevel' = mDevel ++ nl
|| ____________________________________________________________________________
|| :section:    while

while__ = while p f (0, lx)
                where
                p (acc, (x:y:xs)) = x + y <= limit
                f (acc, (x:y:xs)) = (acc + x*y, y:xs)
                limit = 7
                lx = [1,2,3,4,5,6,7,8,9,10]

evals = show ("devel/while  " ++ show (while__))


|| ____________________________________________________________________________
|| :section:    abstype tt

t0  == (string,num,num,string)
t0_show :: t0 -> string
t0_show = show

abstype tt0
with
        tt0_new         :: tt0                                  || new and empty tt
        tt0_append      :: tt0 -> t0 -> tt0                     || append line to tt
        tt0_display     :: tt0 -> [t0]                          || display tt
        tt0_asText      :: tt0 -> text                          || show tt as text
        tt0_show        :: tt0 -> string                        || show tt as string
        tt0_isEmpty     :: tt0 -> bool                          || is tt empty?

tt0 == [t0]

tt0_new                 = []
tt0_append tt0 t        = tt0 ++ [t]
tt0_display tt0         = tt0
tt0_asText              = map t0_show
tt0_show                = text_out . tt0_asText
tt0_isEmpty []          = True
tt0_isEmpty tt0         = False


aTT0 :: tt0
aTT0 = append tt0_new lines
                        where
                        append tt0 (l: ls) = append (tt0_append tt0 l) ls
                        append tt0 [] = tt0
                        lines =
                           [
                                ("otto", 12, 42, "long"),
                                ("max",  15, 72, "short"),
                                ("heinz", 8, 32, "mid"),
                                ("joe",  17, 86, "more")
                           ]


|| evals = mDevel' ++ (tt0_show aTT0) ++ nl ++ show (tt0_isEmpty aTT0)
|| ____________________________________________________________________________
|| :section:    abstype cald
abstype cald
with
        cald_num        :: num -> cald                          || cald from number: 1
        cald_str        :: string -> cald                       || cald from string: "yyyy-mm-dd"
        cald_trip       :: (num,num,num) -> cald                || cald from triple: (yyyy,mm,dd)
        cald_show       :: cald -> string                       || show caldate as string
        cald_diff       :: cald -> cald -> num                  || difference between two days
        cald_add        :: cald -> num -> cald                  || add days to date
||         cald_asTriple   :: cald -> (num,num,num)                || caldate as triple: (yyyy,mm,dd)

cald == num

cald_num n              = n
cald_str                = cald_num . numval
cald_trip (y,m,d)       = cald_num (y *10000 + m * 100 + d)
cald_show               = show
cald_diff a b           = a - b
cald_add d days         = d + days


|| evals = mDevel++"/cald"++nl
||                 ++nl++          cald_show (cald_num 120001)
||                 ++nl++          cald_show (cald_str "123456")
||                 ++nl++          cald_show (cald_trip (34,67,90))
||                 ++nl++ "diff: " ++ show (cald_diff (cald_num 120090) (cald_str "120000"))
||                 ++nl++ "add:"   ++ cald_show (cald_add (cald_num 120090) 8)
||                 ++nl++"---- done"
|| ____________________________________________________________________________
|| :section:    devel
both_eql x x = True
both_eql x y = False

both_succ x (x+100) = True
both_succ x y       = False

ascends (x : (x+1) : (x+2) : (x+3) : xs) = True
ascends xs = False

pred3 0     = 0
pred3 2     = 0
pred3 1     = 0
pred3 (n+3) = n

enums i j = take 100 [ n | n <- i, n+k ..; iseven n ] where k = 2*j


||_____________________________________________________________________________

try *    ::= Ok *   | Failed *


abstype stack *
with    empty   :: stack *
        isempty :: stack * -> bool
        push    :: * -> stack * -> stack *
        pop     :: stack * -> stack *
        top     :: stack * -> *

stack *         == [*]

empty           = []
isempty x       = (x = [])
push a x        = (a:x)
pop (a:x)       = x
top (a:x)       = a


||_____________________________________________________________________________
assocO :: * -> [(*,**)] -> option **
assocO x0 []          = None
assocO x0 ((x,y):xs)  = Some y,         if x = x0
assocO x0 (x:xs)      = assocO x0 xs,   otherwise

assoc1 f v []      = None
assoc1 f v (x:xs)  = Some x,   if f x v = True
assoc1 f v (x:xs)  = assoc1 f v xs

assocwith f []      = None
assocwith f (x:xs)  = Some x,           if f x
assocwith f (x:xs)  = assocwith f xs,   otherwise

assocwith_lc f l = [ (k,v) | (k,v) <- l; f k ]


assoc k []            =  None
assoc k ((ki,v):rest) =  Some (ki,v),   if k = ki
assoc k (x:xs)        =  assoc k xs,    otherwise

assoc_lc k l = [ (ki,v) | (ki,v) <- l; ki = k ]


||_____________________________________________________________________________
lx = [(1,10), (2,20), (3,30), (4,40), (5,50) ]

hx x l =  hdd 1100 [ k+v | (k,v) <- l; k = x ]

|| evals = show lx
||_____________________________________________________________________________
roots a b c = (r1,r2),                 if d >= 0
            = error "no real roots",   otherwise
              where
              r1 =  (-b+r) / (2*a)
              r2 =  (-b-r) / (2*a)
              r  =  sqrt d
              d  =  b^2 - 4*a*c

|| evals = show (roots 2 3 (-12))
||_____________________________________________________________________________
person ::= Person [char] num sex
sex    ::= Female | Male

people = [
                Person "Otto" 42 Male,
                Person "Max"  24 Male,
                Person "Nele" 18 Female
         ]

findPerson people name = [ n | (Person n a g) <- people; n = name ]
findGenders people  gender = [ n | (Person n a g) <- people; g = gender]
findAgeGt people age = [n | (Person n a g) <- people; a > age]

   
|| evals :: string
|| evals = show (findPerson people "Otto")
|| evals = show (findGenders people Male)
|| evals = show (findAgeGt people 3)
||_____________________________________________________________________________
mlens y = [31,feb y,31,30,31,30,31,31,30,31,30,31]
          where
          feb y = 29, if isLeapYear y
                = 28, otherwise


||_____________________________________________________________________________
|| :file: (c) sts-q 2022-Aug

