|| ============================================================================
||
|| :file: test-verbose.m
||
|| ============================================================================
%include "lib.m"
%include "local.m"
%include "xmlgen.m"
%include "pub.m"
%include "test-bignum.m"

|| ============  usage:
||      print testsText:
||              miranda> (testVerbose__)
||
||      write pub/doc1.html to file:
||              (pubDoc1__)

test_Verbose__ :: string
test_Verbose__ = text_out test_VerboseText

test_VerboseText :: text
test_VerboseText =
                [
                   dotAs Section ++ "------------  verbose Tests" ++ dotAs Dbg]

                   ++ test_bignum

                   ++ [dotAs Section ++ "------------  xml_showTag doc1" ++ dotAs Comment]
                   ++ xml_showTag doc1

                   ++ [dotAs Text ++ "--- done" ++ dotAs Std
                ]


|| ============================================================================
|| :chapter: VERBOSE TESTS
|| ----------------------------------------------------------------------------
|| :section:    xmlgen
doc1 :: xml_tag
doc1 =          (xhtml [
                    (xmark_chapter "HEAD"),
                    (xhead [
                        (xmeta [("charset", "utf-8")]),
                        (xmeta [("generator", "Miranda-2.066")]),
                        (xtitle "my-title"),
                        (xmark_section ("inlined style-sheet: " ++ cssFile)),
                        (xinline_styleSheet cssFile)
                    ]),
                    (xmark_chapter "BODY"),
                    (xbody [ (xdiv "contents" [
                        (xmark_section "head"),
                        (xdiv "head" [
                            (xh1 "A first html page."),
                            (xp [Xstr "Created with Pure Functional Programming Language Miranda."]),
                            (xp [Xstr "Created with translation of some special character: <,|,>,&,\",',..."]),
                            (xhr)
                        ]),
                        (xmark_section "body"),
                        (xdiv "body" [
                            (xh1 "headline 1"),
                            (xp  [Xstr "some text in paragraph."]),
                            (xa "dest" "text")
                        ]),
                        (xmark_section "foot"),
                        (xdiv "foot" [
                            (xhr),
                            (xp  [Xstr "Created at ", Xstr today_now, Xstr " with Miranda 2.066 ."]),
                            (xhr)
                        ]),
                    (xmark_chapter "END")
                    ]) ]) ])
                    where
                    cssFile = dirpath_q ++ "css/sts.css"



pubDoc1__ = pubDoc (dirpath_pub ++ "doc1.html") doc1
  

|| ============================================================================
|| (c) sts-q 2022-Aug
