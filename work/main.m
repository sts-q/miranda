#! /home/heiko/bin/mira -exec
|| ============================================================================
|| :file: first.m
|| ============================================================================

%include "../extn/bignum.m"

%include "caldate.m"
%include "devel.m"
%include "items"
%include "lib.m"
%include "json.m"
%include "local.m"
%include "pub.m"
%include "scratch"
%include "xmlgen.m"

%include "test-bignum.m"
%include "test-silent.m"
%include "test-verbose.m"

%include "pub-homepage.m"
%include "pub-plaintext.m"


|| ============================================================================
|| :chapter: MAIN
|| evals :: string
|| evals = show 12 ++ " from main..."
|| evals = main_one

main :: [sys_message]
main =
                [
                   Stdout (dotAs Comment ++ "\n" ++ concat testsText),
                   Stdout (dotAs Prompt  ++ "\n---- evals\n"),
                   Stdout (dotAs Text    ++ evals),
                   Stdout (dotAs Prompt  ++ "\n---- done"),
                   Stdout (dotAs Std)
                ]

main_one :: string
main_one = (text_out . concat)
                [
                   [dotChapter "Miranda main"],
||                    testsVerboseText,

                   [dotChapter "Some results."],
                   ["xterm size: "++ shownum xterm_width ++"x"++  shownum xterm_height],
                   [today_now],
                   [dirpath_home],
                   (lines.triplesFirst) (system "pwd"),
                   [show (map fac [1..7])]
                ]


|| ============================================================================
|| :file:chapter: END sts-q 2022-Jun, 2024-Jul

