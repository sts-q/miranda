||_____________________________________________________________________________
||
|| :file: pub-homepage.m
||
||_____________________________________________________________________________
%include "lib.m"
%include "local.m"
%include "xmlgen.m"
%include "pub.m"

%export pub_homepage__
pub_homepage__ :: [sys_message]                   || write homepage to pub/sts-q.html
pub_homepage__ = pubDoc file html

|| ____________  usage:
||    /f pub-homepage; (pub_homepage)     || write homepage to ~/pub/sts-q.html
||
||_____________________________________________________________________________
title       =  "sts-q"
file        =  dirpath_pub ++ "sts-q.html"
stylesheet  =  dirpath_css ++ "sts.css"

html =
        xhtml [
            xmark_chapter "head",
            xhead [
                xmeta [("charset",     "utf-8")],
                xmeta [("generator",   "Miranda-2066")],
                xtitle title,
                xmark_section "css",
                xinline_styleSheet stylesheet
                ],
            xmark_chapter "body",
            xbody [
                xdiv "contents" [
                    xmark_section "head",
                    xdiv "head" [ xh1 title ],

                    xmark_section "body",
                    xdiv "body" [
                        xmark_sep,


                        xh2 "Miranda",
                        xtext [ "Miranda, the beautiful." ],
                        xdl [
                            xdt [ xa "https://codeberg.org/sts-q/miranda" "Miranda" ],
                            xdd [ xtext ["Repository" ]]],
                        xhr,
                        xmark_sep,


                        xh2 "Oberon",
                        xtext [ "Writing computer programs without stackoverflow." ],
                        xdl [
                            xdt [ xa "https://codeberg.org/sts-q/oberon" "Oberon" ],
                            xdd [ xtext [ "Repository" ]]],
                        xhr,
                        xmark_sep,


                        xh2 "Mezzano",
                        xp [
                                xa "https://github.com/froggey/Mezzano" "Mezzano",
                                Xstr " is an operating system written in Common Lisp, ",
                                Xstr "published by Sylvia Herrington (c)." ],
                        xdl [
                            xdt [ xa "https://sts-q.bitbucket.io/mezzano-cheatsheet.html"
                                      "Mezzano OS Cheatsheet" ],
                            xdd [ xtext [ "is a collection of notes i took, when i started with Mezzano",
                                          "for the first time." ]],
                            xdt [ xa "https://bitbucket.org/sts-q/pet" "Pet" ],
                            xdd [ xtext [ "is a Plain Editor for Text, running on Mezzano OS." ]]],
                        xhr,
                        xmark_sep,


                        xh2 "Minal-OS",
                        xtext [ "An operating system written with Joy." ],
                        xtext [ "A minimal operating system in fasm assembly and programming language Joy." ],
                        xdl [
                            xdt [ xa "https://codeberg.org/sts-q/minal" "Minal-OS" ],
                            xdd [ xtext ["Repository" ]]],
                        xhr,
                        xmark_sep,


                        xh2 "Rabbit-vm",
                        xtext [ "Rabbit-vm is essentially a Joy programming language implementation." ],
                        xtext [
                               "Joy is a functional programming language which is not based ",
                               "on the application of functions to arguments but on the composition ",
                               "of functions. Function composition is done by concatenation. ",
                               "This is called concatenative, tacit or point-free programming ",
                               "or if you prefer say straight away: pointless programming!"],
                        xdl [
                            xdt [
                                xa "https://bitbucket.org/sts-q/rabbit-vm" "Rabbit-vm" ],
                            xdd [ xtext [ "The current implementation of Rabbit-vm" ]],
                            xdt [ xa "https://sts-q.bitbucket.io/rabbit-vm-intro.html" "Introduction" ],
                            xdd [ xtext [ "Introduction to Rabbit-vm." ]]],
                        xhr,
                        xmark_sep,


                        xh2 "Joy Programming Language",
                        xdl [
                            xdt [
                                xa "https://sts-q.codeberg.page/joy-funs-doc.html"
                                   "Atoms and Definitions of JOY" ],
                            xdd [
                                xtext [
                                  "This document is containing a brief description of all Atoms, ",
                                  "defined by the JOY-System and of all visible definitions, ",
                                  "defined by the basic Joy Libraries." ],
                                xtext [
                                  "This document is up to date with Joy as it could be downloaded ",
                                  "from the Joy Home Page at February, 10th 2002." ],
                                xtext [
                                  "It refers to John Cowan's Joy1." ]],
                            xdt [ xa "https://www.kevinalbrecht.com/code/joy-mirror/index.html"
                                     "Joy Programming Language (Mirror)" ],
                            xdd [ xtext [ "A mirror of the Joy homepage by kevinalbrecht." ]],
                            xdt [ xa "https://github.com/Wodan58/joy1" "joy1" ],
                            xdd [ xtext [ "A Joy1 mirror at github by Wodan58." ]]],
                        xhr,
                        xmark_sep,


                        xh2 "vis-editor",
                        xtext [ "Combining Modal Editing with Structural Regular Expressions." ],
                        xdl [
                            xdt [ xa "https://codeberg.org/sts-q/vis-editor" "vis-editor" ],
                            xdd [ xtext ["My configuration files for vis editor." ]]]

                        ],
                    xmark_section "foot",
                    xdiv "foot" [
                        xp [ Xstr "(at) yahoo.de", xbr, Xstr "Heiko. Kuhrt" ],
                        xtext [
                          "(c) First published by sts-q at 2018-12-20 at bitbucket, ",
                          "last change at " ++ today ++ " at codeberg." ]],
                    xmark_chapter "END" ]]]


||_____________________________________________________________________________
|| :file: (c) sts-q 2022-Sep

