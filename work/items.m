||_____________________________________________________________________________
||
|| :file: items.m
||
||_____________________________________________________________________________
%include "lib.m"
%include "caldate.m"

|| evals ||
||_____________________________________________________________________________
|| :chapter: UNITS

unit ::=
                NoUnit
                |Dt |To |Ha |Kg |Ltr |Ml |Stk
                |Euro |Ct
                |Pro unit unit

showUnit :: unit -> string
showUnit (Pro a b)      = show a ++"/"++ show b
showUnit unit           = show unit

|| evals = (concat . (map ((++",").showUnit)))   [Kg, Ha, Ltr, Pro Euro Stk]
||_____________________________________________________________________________
|| :chapter: ABSTYPE AMOUNT mnt
abstype mnt
with
         mnt_of      :: num -> unit -> mnt      || make new amount
         mnt_display :: mnt -> (num, unit)      || display amount
         mnt_show    :: mnt -> string           || show amount
         mnt_add     :: mnt -> mnt -> mnt       || (+)
         mnt_sub     :: mnt -> mnt -> mnt       || (-)
         mnt_mul     :: mnt -> mnt -> mnt       || (*)

mnt == (num, unit)


mnt_of n u              = (n, u)
mnt_show                = show
mnt_display (m,u)       = (m,u)

mnt_add (m,u) (n,p)     = mnt_of (m+n) u, if u = p
                        = error "amount_add: units don't match", otherwise

mnt_sub (m,u) (n,p)     = mnt_of (m-n) u, if u = p
                        = error "amount_sub: units don't match", otherwise

mnt_mul (m,u) (n,p)     = mnt_of (m*n) u, if u = p
                        = error "amount_mul: units don't match", otherwise


|| evals =         "items.m :section: abstype/amount"++nl
||                 ++nl++ mnt_show (mnt_add (mnt_of 102 Ha) (mnt_of 32 Ha))
||                 ++nl++ mnt_show (mnt_sub (mnt_of 102 Ha) (mnt_of 32 Ha))
||                 ++nl++ mnt_show (mnt_mul (mnt_of 102 Ha) (mnt_of 32 Ha))

|| evals :: string
||_____________________________________________________________________________
|| :chapter: ITEMS
item ::=
                NoDef
                | Num num
                | NaN
                | Str string
                | Txt text
                | Bool bool
                | Caldate caldate
                | Date num
                | Cent num
                | Price num unit
                | Err string


showItem :: num -> item -> string
showItem w NoDef           = cjustify w "<NODEF>"
showItem w (Num n)        =  rjustify w (shownum n)
showItem w (NaN)          =  cjustify w "<NAN>"
showItem w (Str s)        =  take w s
showItem w (Txt t)        =  inconcat nl t
showItem w (Bool True)    =  cjustify w "True"
showItem w (Bool False)   =  cjustify w "False"
showItem w (Caldate d)    =  rjustify w (showISODate d)
showItem w (Date d)       =  rjustify w (showISODate (asCalDate d))
showItem w (Cent n)       =  rjustify w (showeuro n)
showItem w (Price p u)    =  rjustify w (showeuro p) ++" "++ showUnit u
showItem w (Err mess)     =  "#######  " ++ take (w-4) mess


item1 =
                [
                        NoDef,
                        Num 17,
                        Num 3.10020033,
                        NaN,
                        Str "Hello, this is Miranda, the beautyfull.",
                        Txt ["Hello", "world", "Maxottokarl"],
                        Bool True, Bool False,
                        Caldate (2022,8,24),
                        Date (asDayNum (2055,5,20)),
                        Cent 10012,
                        Cent 910010012,
                        Price 1250 Dt,
                        Price 211 Ltr,
                        Price 911211 Ltr,
                        Price 164 (Pro Ct Ltr),
                        Err "Something went wrong...!",
                        NoDef
                ]

items__ = lay (map (showItem 11) item1)


|| evals = text_out (map (showItem 25) item1)
||_____________________________________________________________________________
|| :file: (c) sts-q 2022-Aug


