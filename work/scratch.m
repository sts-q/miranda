
%include "lib.m"


succ n          = n+1
pred n          = n - 1
twice f x       = f (f x)
factors n       = [i | i <- [1..n div 2]; n mod i = 0]
ones            = 1:ones
twos            = [2,2..]
perfects        = [ n | n <- [1..]; sum (factors n) = n]

lookup n        = [ "<" ++ shownum s ++ ">" | s <- [0..]] ! n

(foo,bar) = (+,-)
foo_bar = foo 12 4 + 10 $bar 3 * 1000

run                     = concat [snd s ++ "\nNext: " | s <- states]
states                  = scan do start $+
start                   = (0, "Please type in numbers.")
do (n,cs) m             = (n+m, "Total is "++ show (n+m))

fib_500 = fib 500
fib_600 = fib 600
fib_mess = "hallo, fib_500 is" ++ show fib_500 ++ ", a large number we will never see..."

age name = of name where of "max"=12; of "joe"=22; of "henry"=43; of "mike"=83

xor :: bool -> bool -> bool
xor a b = (a \/ b) & ~(a & b)

|| (t:ts, (ta,tta:ttas)) = ([1,2,3,4], (5,6:[7,8,9]))    || yes, works






