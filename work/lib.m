||_____________________________________________________________________________
||
|| :file: lib.m
||
|| nl
||_____________________________________________________________________________
|| prod ||
mLib = "lib"                                                                    || #DOC miralib
scomp = "scomp"                                                                 || #DOC miralib

string    == [char]                                                             || #DOC types
text      == [string]                                                           || #DOC types
name      == string                                                             || #DOC types
filename  == string                                                             || #DOC types

option * ::= Some * | None                                                      || #DOC types


||_____________________________________________________________________________
|| :chapter: DOT
brown           =  "240;200;100"
lightgray       =  "128;128;128"
gray            =  "96;96;96"
magenta         =  "128;128;0"
red             =  "255;0;0"
sky             =  "80;128;230"
white           =  "255;255;255"

csi str         = '\027' : '[' : str
osc str         = '\027' : ']' : str ++ ['\007']
dotIn rgb       = csi ("38;2;" ++ rgb ++ "m")
dotClr          = csi "2J" ++ csi "1;1H" ++ dotAs Text ++ dot_cursorInMagenta

motstyle ::= Section | Text | Comment | Dbg | Error | Std | Prompt

dotAs Section   = dotIn brown
dotAs Error     = dotIn red
dotAs Text      = dotIn lightgray
dotAs Comment   = dotIn gray
dotAs Dbg       = dotIn magenta
dotAs Prompt    = dotIn sky
dotAs Std       = dotIn white

dot_cursorInRed     = osc "12;red"
dot_cursorInGreen   = osc "12;green"
dot_cursorInBlue    = osc "12;blue"
dot_cursorInWhite   = osc "12;white"
dot_cursorInYellow  = osc "12;yellow"
dot_cursorInMagenta = osc "12;magenta"
dot_cursorInCyan    = osc "12;cyan"
dot_cursorInBlack   = osc "12;black"
dot_cursorInGray    = osc "12;gray"

clr             = lay [dotClr]

dotChapter name = dotAs Section ++ "============  " ++ name ++ dotAs Text
dotSection name = dotAs Section ++ "------------  " ++ name ++ dotAs Text


||_____________________________________________________________________________
|| :chapter: DISPLAY

display :: [text] -> [sys_message]
display texts = map f (appendToLast (dotAs Std) (concat texts))
                where f l = Stdout (l ++ nl)

chapter mess = dotAs Section ++ "\n\n============  " ++ mess ++ dotAs Text ++ "\n"
section mess = dotAs Section ++ "\n------------  " ++ mess ++ dotAs Text ++ "\n"

xterm_width  = (numval.hd.lines.triplesFirst) (system "tput cols")
xterm_height = (numval.hd.lines.triplesFirst) (system "tput lines")


||_____________________________________________________________________________
|| :chapter: MATH
inc n           = n + 1
dec n           = n - 1

fac 0           = 1
fac n           = product [1..n]

fac_2 :: num -> num
fac_2 0 = 1
fac_2 n = n * fac_2 (n-1)

fib_1 0         = 0
fib_1 1         = 1
fib_1 n         = fib_1 (n-1) + fib_1 (n-2)

fib_2 0 = 0
fib_2 1 = 1
fib_2 (n+2) = fib_2 (n+1) + fib_2 n

fib_3 0 = 0
fib_3 1 = 1
fib_3 (n+1) = fib_3 (n) + fib_3 (n-1)

fibs = [a | (a,b) <- (0,1), (b, a+b) ..]
fib_4 = (fibs!)

fib = fib_1


ack 0 n         = n+1
ack (m+1) 0     = ack m 1
ack (m+1) (n+1) = ack m (ack (m+1) n)
ack m n         = error "ack applied to -ve or fractional arg"

collatzChain 1  =  [1]
collatzChain n  =  n : collatzChain (n div 2),   if iseven n
collatzChain n  =  n : collatzChain (3*n+1),     otherwise

divisors n      = [ ds | ds <- [2..n div 2]; n mod ds = 0 ]
primes          = [ ps | ps <- [1..]; divisors ps = []]
isprime n       = divisors n = []

|| EUCLID(a,b)
|| 1  wenn b = 0
|| 2      dann return a
|| 3  sonst return EUCLID(b, a mod b)
|| gcd {i i -- i  // a b -> gcd}
        || | 0 ==* | zap
        || others  | swap   over mod rec
gcd a 0         = a
gcd a b         = gcd b (a mod b)

lcm a b         = a * b / gcd a b

sqr n           = n * n
iseven n        = n mod 2 = 0
isodd n         = n mod 2 ~= 0

sgn n           =  1,  if n > 0
sgn 0           =  0
sgn n           = -1,  otherwise

arith_mean ln   = sum ln / #ln
geom_mean ln    = (product ln) ^ (1 / #ln)

rnd num         =      entier ( num + 0.5),   if num >= 0
                = neg (entier (-num + 0.5)),  otherwise


random_max    = 31415821
random_next n = (n * b + 1) mod random_max
                where
                b = 10^8

random_roll init limit  = [ n mod limit | n <- iterate random_next init ]
random_boolean init     = [ isodd n     | n <- iterate random_next init ]
random_ppt init limit   = [ n < limit   | n <- random_roll init 1000]           || parts per thousand

random_shuffle :: num -> [*] -> [*]
random_shuffle init l   =
                do l
                where
                do []     = []
                do [x]    = [x]
                do  l     = l!rdx : do (cutout rdx l)
                            where
                            rdx = rndindex l
                cutout i l = take i l  ++  drop (i+1) l
                rndindex l = next rnums  mod  #l
                rnums      = random_roll init random_max


shownum00 i     = shownum i,                    if i > 9
                = error "shownum00: i < 0",     if i < 0
                = '0' : [decode (i + 48)],      otherwise

showval n name  = "   " ++ shownum n ++ " " ++ name


||_____________________________________________________________________________
|| :chapter: CURRENCY
readeuro :: string -> num                       || read 110,12 as 11012 cent
readeuro s
   = cent
      where
      cent = numval (filter ((~=)',') s)


showeuro :: num -> string                       || show 10012 cent as 100.12
showeuro cent
   = error mess,        if ~(integer cent)
   = eus ++ "." ++ cts, otherwise
      where
      mess      =  "showeuro: integer expected: " ++ shownum cent
      (eus,cts) =  rive (#s-2) s
      s         =  shownum cent


||_____________________________________________________________________________
|| :chapter: FLOATS
flt_epsilon     = 0.0000001
flt_eql r s     = abs (r-s) < flt_epsilon


||_____________________________________________________________________________
|| :chapter: CHARACTERS
char_downcase :: char -> char                                   || downcase char
char_downcase c  =  decode (code c + 32),   if 'A' <= c <= 'Z'
                 =  c,                      otherwise

char_isletter :: char -> bool                                   || is char a letter
char_isletter = letter

char_isdigit :: char -> bool                                    || is char a digit
char_isdigit  = digit


char_upcase :: char -> char                                     || upcase char
char_upcase c    =  decode (code c - 32),   if 'a' <= c <= 'z'
                 =  c,                      otherwise



||_____________________________________________________________________________
|| :chapter: COMBINATORS
compose22 f g  a b c    = f (g a b) c           || f takes 2, g takes 2 arguments
compose21 f g  a b      = f (g a) b             || f takes 2, g takes 1 argument

dip f g   a b c         = f a (g b c)           || simple dip: two binary operators, applies g first

unpair (a,b)            = a b
apptopair f (a,b)       = f a b
zap a b                 = b
nip a b                 = a

times :: num -> (* -> *) -> * -> *
times n f x             = x,   if n <= 0
times n f x             = times (n-1) f (f x)

while :: (*->bool) -> (*->*) -> * -> *
while p f x = while p f (f x),   if p x
            = x,                 otherwise


||_____________________________________________________________________________
|| :chapter: TUPLES
triplesFirst (a,b,c)    = a
triplesSecond(a,b,c)    = b
triplesThird (a,b,c)    = c


||_____________________________________________________________________________
|| :chapter: LISTS
nl              = "\n"
no l            = l = []
some l          = l ~= []
|| swoncat l m     = m ++ l
swoncat         = converse (++)
|| join l x        = l ++ [x]
join            = postfix

|| append a list to the last element of a list of lists
appendToLast :: [*] -> [[*]] -> [[*]]
appendToLast l [] = [l]
appendToLast l ll =
                reverse (append (reverse ll) l)
                where
                append (l : ls) tail = (l ++ tail) : ls

consToFirst :: * -> [[*]] -> [[*]]
consToFirst x (xs : xss) = (x :xs) :xss
consToFirst x [] = error "consToFirst: no first list in xss"

|| inlining concatenation of lists: concat list elements with lin inlined
inconcat lin []         = []
inconcat lin (l : [])   = l
inconcat lin (l : ls)   = l ++ lin ++ (inconcat lin ls)


hdd :: * -> [*] -> *                            || hd with given default in case of []
hdd default l  =  hd (l ++ [default])


|| wrap list with pre- and postfix sequences
wrapconcat pre post l   = pre ++ l ++ post

|| flatten a list of lists
|| flatten = concat

hasHead h l     =   h = (take (#h) l)
hasTail t l     =   t = (drop (#l - #t) l)


hasSubseq s l = do l
                where
                len = #s
                do [] = False
                do l  = False,       if #l < len
                      = True,        if s = take len l
                      = do (tl l),   otherwise


findSubseq s []         = (False, [])
findSubseq s l          = (False, []),      if #s > #l
findSubseq s l          = (True,   l),      if s = take (#s) l
findSubseq s (x:xs)     = findSubseq s xs,  otherwise

next :: [*] -> *
next = hd

alist * ::= ANode * | AList [alist *]
parseAList :: alist * -> alist * -> alist * -> (alist * ,alist *)
parseAList start stop (AList (start : ts)) =
                (AList tree, AList rest) where
                (tree, rest) = parse [] [] ts
                parse l              nl     []          = (reverse nl, [])
                parse []             nl  (t : ts)       = (reverse nl, ts),                       if t = stop
                parse l              nl  (t : ts)       = parse (AList nl:l)    []            ts, if t = start
                parse (AList l1:ls)  nl  (t : ts)       = parse ls              (AList (reverse nl):l1) ts, if t = stop
                parse l              nl  (t : ts)       = parse l               (t:nl)        ts

parseAList start stop (AList[]) = (AList [], AList [])
parseAList start stop alist = error ("parseAList: alist must start with start-node")

parseAList__ =
   lay [
        show (parseAList (ANode 1)   (ANode 9)   (AList [ANode 1, ANode 2, ANode 9])),
        show (parseAList (ANode 1)   (ANode 9)   (AList (map ANode [1,2,  1,3,9,  4,9]))),
        show (parseAList (ANode '[') (ANode ']') (AList (map ANode text_1))),
        show (parseAList (ANode '(') (ANode ')') (AList (map ANode text_2)))
       ]
   where
   text_1 = "[hallo[word[end]]]tail"
   text_2 = "(3*(4+5*(1+2)))"


removeWith :: (* -> bool) -> [*] -> [*]
removeWith p (x:xs) = xs, if p x
                    = x : removeWith p xs, otherwise
removeWith p [] = []

rive :: num -> [*] -> ([*],[*])
rive i l = (take i l,drop i l)

rolldown :: [*] -> [*]
rolldown []     = []
rolldown (x:xs) = xs ++ [x]

rollup :: [*] -> [*]
rollup []  =  []
rollup l   =  last l : init l

nub :: [*] -> [*]                               || remove duplicates, keep first
nub l  =  do [] l
                where
                do m []      =  reverse m
                do m (x:xs)  =  do m xs,       if member m x
                do m (x:xs)  =  do (x:m) xs,   otherwise

uniq :: [*]->[*]    ||removes adjacent duplicates from a list (found in ex/set.m)
uniq (a:b:x) =  uniq (a:x),   if a=b
             =  a:uniq (b:x), otherwise
uniq x       =  x


||_____________________________________________________________________________
|| :chapter: STRINGS
downcase :: string -> string
downcase s = map char_downcase s


upcase :: string -> string
upcase s  =  map char_upcase s


||_____________________________________________________________________________
|| :chapter: TEXT
text_out :: text -> string                      || concat text to string, append newline to each line
text_out = lay
|| text_out text =
||                 foldl f "" text
||                 where
||                 f t ts = t ++ ts ++ "\n"

unwords :: [string] -> string
unwords (w : []) = w
unwords (w : ws) = w ++" "++ unwords ws
unwords [] = ""


||_____________________________________________________________________________
|| :chapter: DICTIONARIES
kvp  * **   ==   (*,**)                         || kvp:        a key-value-pair
dict * **   ==   [kvp * **]                     || dictionary: a kvp list

dict_keys   :: dict * ** -> [*]                 || get sorted list of keys
dict_values :: dict * ** -> [**]                || get sorted list of values
dict_get    :: dict * ** -> * -> option **      || get first kvp where k == **
dict_getKey :: dict * ** -> ** -> option *      || get first kvp where v == **
dict_getKeyWith :: (**->***->bool) -> dict * ** -> *** -> option *


dict_keys   d   =  sort (map fst d)
dict_values d   =  sort (map snd d)

dict_get d k    = None,           if vs = []
                = Some (hd vs),   otherwise
                  where
                  vs = [ v | (ki,v) <- d; ki = k ]

dict_getKey d v = None,           if ks = []
                = Some (hd ks),   otherwise
                  where
                  ks = [ k | (k,vi) <- d; vi = v ]

dict_getKeyWith f d v
                = None,           if ks = []
                = Some (hd ks),   otherwise
                  where
                  ks = [ k | (k,vi) <- d; f vi v ]


||_____________________________________________________________________________
|| :chapter: TABLES
fmtTT :: [[string]] -> text                     || all record fields are strings -> text
fmtTT []  = []
fmtTT tt  =
                map (wrapconcat "|" "|") (map (inconcat "|") (map fmtXs tt))
                where
                fmtXs xs     = map2 justify   widths xs
                widths       = map max  (map (map (#)) (transpose tt))
                justify w cs = rjustify w cs


|| fmt every record of TT with g: (some-tuple) -> [string]
fmtTTwith :: (* -> [string]) -> [*] -> text
fmtTTwith g tt = fmtTT (map g tt)


|| fmt every field of TT with g: some-value -> string
fmtTTover :: (* -> string) -> [[*]] -> text
fmtTTover g tt = fmtTT (map (map g) tt)


||_____________________________________________________________________________
|| :chapter: CSV

csv_readFields    :: char -> string -> [string]
csv_readFields delimiter l  =
                do [] l
                where
                do w (c:cs)  =  reverse w : do [] cs,   if c = delimiter
                do w (c:cs)  =  do (c:w) cs
                do w []      =  [reverse w]


||_____________________________________________________________________________
|| :chapter: FILES
dirpath_home    = getenv "HOME"


||_____________________________________________________________________________
|| :chapter: TIME AND DATE
current_second = (numval.hd.lines.triplesFirst) (system "date +%s")

today         = take 10 d
                where
                (d,errmess,err) = system "date +%F"

now           = take 8 t
                where
                (t,errmess,err) = system "date +%T"

today_now     = today ++ " " ++ now



||_____________________________________________________________________________
|| :chapter: SYSTEM

sleep n       = err
                where
                (mess,errmess,err) = system ("sleep " ++ (shownum n))


etime :: (* -> **) -> * -> (num, **)            || execution time in seconds
etime f x =
                (t,res)
                where
                res = f x
                t   = v done_time - v start_time
                (start_time,errmess1,err1) =          system "date +%s"
                (done_time, errmess2,err2) = seq res (system "date +%s")
                v   = numval.hd.lines


||_____________________________________________________________________________
|| :chapter: HELP

dda    name = triplesFirst (system ("mira -exports " ++ name ++ ".m"))
ddcont name = triplesFirst (system ("egrep ':chapte"++"r:|:s"++"ection:' " ++ name ++ ".m"))
dds    name = triplesFirst (system ("egrep -n '^ *" ++ name ++ "_.* ::' * "))
ddoc   name = triplesFirst (system ("clear; egrep " ++ patt ++ name ++ ".m"))
                where
||                 patt = " '#DOC|:file:|:chapter:|:section:' "
                patt = " '#DOC' "

help = lay [
        "============  mira ",
        "       $> ./now main           run main as script file: print result of function 'main'",
        "       $> ./now repl           start mira repl: eval 'main' to get result of function 'main'",
        "",
        "       dda \"lib\"               dot module api",
        "       dda mLib                dot module api (if mLib = 'lib')",
        "       ddcont mLib             dot module contents",
        "       dds \"name\"              dot defintions with prefix 'name_' e.g. dds 'text'",
        "       ddoc 'file'             grep #DOC from file                     mira> ddoc 'scomp'",
        "",
        "       $$                      eval last expression",
        "       id ::                   print type info",
        "       ?                       list all definitions",
        "       ? id                    print info about this definition",
        "       ?? id                   show definition in editor",
        "       /f %                    recompile current script",
        "       /e                      edit file",
        "       !cmd                    exec shell command",
        "       !!                      repeat last shell command"
        ]


||_____________________________________________________________________________
|| :file:  (c) sts-q 2022-Aug, 2024-Jul

