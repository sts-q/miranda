||_____________________________________________________________________________
||
|| :file: xmlgen.m
||
||_____________________________________________________________________________
%include "lib.m"

%export
   xml_tag
   xml_showTag

xml_showTag :: xml_tag -> text                                  || format xml_tag to text
xml_showTag     = showTag

||_____________________________________________________________________________
|| :chapter: TYPES
attrName        == string
attrValue       == string
tagName         == string

attrTuple       == (attrName,attrValue)

xml_tag  ::=    Xtag   tagName  [attrTuple]  [xml_tag]          || algebraic data type for xml tags
                | Xstr string
                | Xmark_chapter string
                | Xmark_section string
                | Xmark_separator


||_____________________________________________________________________________
|| :chapter: XMLGEN

translateSpecialChars :: string -> string
translateSpecialChars s =
                do s
                where
                do ""           =  ""
                do ('<'  : cs)  =  "&lt;"   ++ do cs
                do ('>'  : cs)  =  "&gt;"   ++ do cs
                do ('&'  : cs)  =  "&amp;"  ++ do cs
                do ('"'  : cs)  =  "&quot;" ++ do cs
                do ('\'' : cs)  =  "&apos;" ++ do cs
                do (c    : cs)  =  c        :  do cs


showAtts :: [attrTuple] -> string
showAtts []  =  ""
showAtts ((name,value) : atts)  =  " "++ (name ++ "=\"" ++ value ++ "\"") ++ showAtts atts


showEmptyTag name atts  =  ["\n<"++ name ++ showAtts atts ++" />"]

showStdTag name atts ts =
                startTag name atts ++ (concat(map showTag ts)) ++ stopTag name
                where
                startTag name atts  =  ["\n<"++ name ++ showAtts atts ++">"]
                stopTag  name       =  ["</" ++ name ++ ">"]


showTag :: xml_tag -> [string]

showTag (Xtag name atts [])  =  showEmptyTag name atts

showTag (Xtag name atts ts)  =  showStdTag   name atts ts

showTag (Xstr str)           =  [translateSpecialChars str]

showTag (Xmark_chapter name) =
          ["\n\n\n<!-- =============================================================================== -->\n",
           "<!-- :chapter:  " ++ name ++ " -->\n\n"]

showTag (Xmark_section name) =
          [  "\n\n<!-- _______________________________________________________________________________ -->\n",
           "<!-- :section:  " ++ name ++ " -->\n"]

showTag (Xmark_separator)  =  [ "\n\n" ]

||_____________________________________________________________________________
|| :file: (c) sts-q 2022-Aug

