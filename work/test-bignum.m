|| ============================================================================
||
|| :file: test-bignum.m
||
|| ============================================================================
%include "lib.m"
%include "../extn/bignum.m"

|| ============================================================================

test_bignum :: text
test_bignum =
                [       dotSection "bignum",
                        show (bn_is_zero (bn_0)),
                        take len (showbignum (bn_div (bn_1) (bn_2))),
                        take len (showbignum (bn_div (bn "1") (bn "3"))),
                        take len (showbignum (bn_pi)),
                        take len (showbignum (bn_sqrt (bn_2)))
                    ]
                where 
                len = xterm_width


|| ============================================================================
|| sts-q 2022-Aug

